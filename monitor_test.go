package monitor

import (
	"bytes"
	"sync"
	"testing"

	"gitlab.com/NebulousLabs/fastrand"
)

// TestMSimpleWriteRead tests a simple monitored write and read operation.
func TestMSimpleWriteRead(t *testing.T) {
	// Create monitor
	m := NewMonitor()

	// Create a io.ReadWriter.
	rw := bytes.NewBuffer(make([]byte, 0))

	// Wrap it into a monitored counter.
	mc := NewMCounter(rw, m)

	// Create 1mb to write.
	data := fastrand.Bytes(1000)

	// Write data
	n, err := mc.Write(data)

	// Check for errors
	if n < len(data) {
		t.Error("Not whole data was written")
	}
	if err != nil {
		t.Error("Failed to write data", err)
	}
	// Check the counter was incremented
	if m.writeBytes != data {
		t.Errorf("Expected writeBytes to be %v but was %v", data, m.writeBytes)
	}

	// Read data back from file while measuring time.
	readData := make([]byte, len(data))
	n, err = mc.Read(readData)

	// Check for errors
	if n < len(data) {
		t.Error("Not whole data was read")
	}
	if err != nil {
		t.Error("Failed to read data", err)
	}
	// Check the counter was incremented
	if m.readBytes != data {
		t.Errorf("Expected readBytes to be %v but was %v", data, m.readBytes)
	}
	// Check if the read data is the same as the written one.
	if bytes.Compare(readData, data) != 0 {
		t.Error("Read data doesn't match written data")
	}
}

// TestMParallelWriteRead tests a parallel monitored write and read operations.
func TestMParallelWriteRead(t *testing.T) {
	// Create monitor
	m := NewMonitor()
	bytesToWrite := int(1000)

	// f creates a monitored buffer, writes some data to it and reads it
	// afterwards.
	f := func() {
		// Create a io.ReadWriter.
		rw := bytes.NewBuffer(make([]byte, 0))

		// Wrap it into a monitored ReadWriter.
		mc := NewMCounter(rw, m)

		// Create 1mb to write.
		data := fastrand.Bytes(bytesToWrite)

		// Write data while measuring time.
		n, err := mc.Write(data)

		// Check for errors
		if n < len(data) {
			t.Error("Not whole data was written")
		}
		if err != nil {
			t.Error("Failed to write data", err)
		}

		// Read data back from file while measuring time.
		readData := make([]byte, len(data))
		n, err = mc.Read(readData)

		// Check for errors
		if n < len(data) {
			t.Error("Not whole data was read")
		}
		if err != nil {
			t.Error("Failed to read data", err)
		}
		// Check if the read data is the same as the written one.
		if bytes.Compare(readData, data) != 0 {
			t.Error("Read data doesn't match written data")
		}
	}
	// Start a few threads and wait for them to finish.
	var wg sync.WaitGroup
	numThreads := 10
	for i := 0; i < numThreads; i++ {
		wg.Add(1)
		go func() {
			f()
			wg.Done()
		}()
	}
	wg.Wait()

	// Check the counters was incremented
	totalBytes := numThreads * bytesToWrite
	if m.writeBytes != totalBytes {
		t.Errorf("Expected writeBytes to be %v but was %v", totalBytes, m.writeBytes)
	}
	if m.readBytes != totalBytes {
		t.Errorf("Expected readBytes to be %v but was %v", totalBytes, m.readBytes)
	}
}
