package monitor

import (
	"fmt"
	"io"
	"net"
	"sync"
)

type (
	// Monitor is a simple byte counter for read and write operations on a
	// io.ReadWriter.
	Monitor struct {
		writeBytes uint64 // bytes that have been written.
		readBytes  uint64 // bytes that have been read.

		mu sync.Mutex
	}

	// mCounter is a simple counter wrapper for the io.ReadWriter
	// interface.
	mCounter struct {
		io.ReadWriter
		m *Monitor
	}
	// mConn is a simple counter wrapper for the net.Conn interface.
	mConn struct {
		net.Conn
		mc mCounter
	}
)

// NewMonitor creates a new counter object that can be used to initialize
// monitored readers and writers.
func NewMonitor() *Monitor {
	return &Monitor{
		writeBytes: 0,
		readBytes:  0,
	}
}

// NewMCounter wraps a io.ReadWriter into a mCounter.
func NewMCounter(rw io.ReadWriter, m *Monitor) io.ReadWriter {
	return &mCounter{
		rw,
		m,
	}
}

// NewMConn wrap a net.Conn into a mConn.
func NewMConn(conn net.Conn, m *Monitor) net.Conn {
	return &mConn{
		Conn: conn,
		mc: mCounter{
			ReadWriter: conn,
			m:          m,
		},
	}
}

// Counts gets the current counts for the Monitor.
func (m *Monitor) Counts() (uint64, uint64) {
	m.mu.Lock()
	defer m.mu.Unlock()
	return m.writeBytes, m.readBytes
}

// Read is a pass-through to the mCounter's monitored Read method.
func (c *mConn) Read(b []byte) (n int, err error) { return c.mc.Read(b) }

// Write is a pass-through to the mCounter's monitored Read method.
func (c *mConn) Write(b []byte) (n int, err error) { return c.mc.Write(b) }

// Read reads from the underlying readWriter and counts the bytes read.
func (mc *mCounter) Read(b []byte) (n int, err error) {
	mc.m.mu.Lock()
	mc.m.readBytes += uint64(len(b))
	fmt.Println("Reading", len(b), "bytes")
	mc.m.mu.Unlock()
	return mc.ReadWriter.Read(b)
}

// Write writes from the underlying readWriter and counts the bytes written.
func (mc *mCounter) Write(b []byte) (n int, err error) {
	mc.m.mu.Lock()
	mc.m.writeBytes += uint64(len(b))
	fmt.Println("Writting", len(b), "bytes")
	mc.m.mu.Unlock()
	return mc.ReadWriter.Write(b)
}
